
n, m = list(map(int, input().split()))
matrix = [[(i + 1) * (j + 1) for j in range(m)] for i in range(n)]
for vector in matrix:
    print(*vector)
