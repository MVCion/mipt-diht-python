def primes_generator():

    def is_prime(number):
        for divider in range(2, int(number**0.5) + 1):
            if number % divider == 0:
                return False
        return True

    def next_prime(number):
        number += 1
        while not is_prime(number):
            number += 1
        return number

    prime = 2
    while True:
        yield prime
        prime = next_prime(prime)


if __name__ == '__main__':
    prime_index = int(input())
    nth_prime = primes_generator()
    for i in range(prime_index - 1):
        next(nth_prime)
    print(next(nth_prime))
