from collections import defaultdict


def palindromable(template):
    centered, counter = False, defaultdict(int)
    for char in template:
        counter[char] += 1
    for entries in counter.values():
        if entries % 2 == 1 and not centered:
            centered = True
        elif entries % 2 == 1 and centered:
            return False
    return True


if __name__ == '__main__':
    template = input()
    print("YES" if palindromable(template) else "NO")
