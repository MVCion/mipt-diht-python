
import re
from collections import defaultdict


def parse(translation):
    translation = re.sub('[^A-Za-z ]+', '', translation).split()
    return translation[0], translation[1:]


words, translations = int(input()), defaultdict(list)
for _ in range(words):
    key, values = parse(input())
    translations[key] = values
reverseddict = defaultdict(list)
for key, values in translations.items():
    for word in values:
        reverseddict[word].append(key)
print(len(reverseddict))
for key in sorted(reverseddict.keys()):
    print("{0} - {1}".format(key, ", ".join(sorted(reverseddict[key]))))
