def predict_events(events, happy, sad):
    happiness_pts, sadness_pts = 0, 0
    for event in events:
        if event in happy:
            happiness_pts += 1
        if event in sad:
            sadness_pts += 1
    return (happiness_pts, sadness_pts)

events = list(input().split())
happy, sad = [set(input().split()) for _ in range(2)]
happiness_pts, sadness_pts = predict_events(events, happy, sad)
print(happiness_pts - sadness_pts)
