def force_load(module_name):

    from traceback import extract_tb
    from sys import exc_info

    force_executable = dict()
    with open("%s.py" % module_name, 'r+') as module:
        program = module.readlines()

    def execute():
        nonlocal program, force_executable
        exec("".join(program), globals(), force_executable)

    while True:
        try:
            execute()
            break
        except SyntaxError as syntax_error:
            del program[syntax_error.lineno - 1]
        except BaseException:
            _, not_breakpoint, trace = exc_info()
            del program[extract_tb(trace)[-1][1] - 1]
            if not not_breakpoint:
                break

    return force_executable
