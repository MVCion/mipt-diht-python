def type1(request):
    valid = True

    def convert(number):
        nonlocal valid
        if 0 <= ord(number[0]) - ord('a') <= 5:
            return ord(number[0]) - ord('a') + 10
        valid = False
        return -1

    request = request.lower()
    r = 16 * convert(request[0]) + convert(request[1])
    g = 16 * convert(request[2]) + convert(request[3])
    b = 16 * convert(request[4]) + convert(request[5])
    if valid:
        return "%d %d %d" % (r, g, b)
    else:
        return "ERROR"


def type2(request):
    order, values, valid = {}, {}, True
    for char in "rgb":
        order[char] = request.find(char)
    if "%" in request:
        for i, value in enumerate(request[4:-1].split(",")):
            try:
                values[i] = int(2.55 * float(value[:-1]) + 0.001)
                if not(0 <= values[i] <= 255):
                    valid = False
            except ValueError:
                valid = False
    else:
        for i, value in enumerate(request[4:-1].split(",")):
            try:
                values[i] = int(value)
                if not(0 <= values[i] <= 255):
                    valid = False
            except ValueError:
                valid = False
    if valid:
        return "%d %d %d" % \
               (values[order['r']], values[order['g']], values[order['b']])
    else:
        return "ERROR"


def type3(request):
    valid = True
    for value in request.split(','):
        try:
            if 0 <= int(value) <= 255:
                continue
            else:
                valid = False
        except ValueError:
            valid = False
    if valid:
        return request.replace(",", " ")
    else:
        return "ERROR"


request = input().replace(" ", "").lower()

if request.lstrip()[0] == '#':
    response = type1(request[1:])
elif set("rgb()") & set(request) == set("rgb()"):
    response = type2(request)
elif len(request.split(",")) == 3 and \
        not set("rgb()") & set(request) == set("rgb()"):
    response = type3(request)
else:
    response = "ERROR"

print(response)
