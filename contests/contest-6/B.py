def prepare_to_british_scientists(text, letters_to_shuffle_nm):
    import re

    def switch(match_object):
        import random
        word = list(match_object.group(1))
        idx = min(letters_to_shuffle_nm + 1, len(word) - 1)
        if idx > 2:
            to_modify = word[1:idx]
            random.shuffle(to_modify)
            word[1:idx] = to_modify
        return "".join(word)

    return re.sub(r"(\w+)", switch, text)
