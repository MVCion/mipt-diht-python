import contextlib


@contextlib.contextmanager
def supresser(*args):
    try:
        yield
    except args:
        pass


@contextlib.contextmanager
def retyper(type_from, type_to):
    try:
        yield
    except type_from as base_exception:
        exception = type_to()
        exception.args = base_exception.args
        exception.__traceback__ = base_exception.__traceback__
        raise exception


@contextlib.contextmanager
def dumper(stream):
    try:
        yield
    except Exception as exception:
        stream.write(str(exception))
        stream.write(str(exception.__traceback__))
        raise exception

