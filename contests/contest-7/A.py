def strings(file_name, min_str_len=4):
    import string
    printable = set(list(map(ord, string.printable)))

    def check(file_line):
        buffer = ""
        for sign in file_line:
            if sign in printable:
                buffer += str(chr(sign))
            else:
                buffer = buffer.strip()
                if len(buffer) > min_str_len:
                    yield buffer
                buffer = ""

        buffer = buffer.strip()
        if len(buffer) > min_str_len:
            yield buffer

    with open(file_name, 'rb') as file:
        for line in file:
            for printable_str in check(line):
                yield printable_str
