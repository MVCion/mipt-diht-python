def flatit(obj):
    flatted = list()

    def flat(outter):
        nonlocal flatted
        if isinstance(outter, str):
            for sign in outter:
                flatted.append(sign)
        else:
            if hasattr(outter, '__iter__'):
                for inner in outter:
                    flat(inner)
            else:
                flatted.append(outter)

    flat(obj)
    for element in flatted:
        yield element
