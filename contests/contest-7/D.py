import functools as f
import itertools as it
import operator


def transpose(matrix):
    return [x for x in zip(*matrix)]


def uniq(elements):
    return list(set(elements))


def dict_merge(*args):
    return f.reduce((lambda x, y: dict(it.chain(x.items(), y.items()))), args)


def product(a, b):
    try:
        return f.reduce((lambda x, y: x + y), map(operator.mul, a, b))
    except TypeError:
        return 0
