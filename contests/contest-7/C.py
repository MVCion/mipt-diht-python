class Range:

    def __init__(self, *args):
        if len(args) == 1:
            self.start, self.stop, self.step = 0, args[0], 1
        elif len(args) == 2:
            self.start, self.stop, self.step = args[0], args[1], 1
        elif len(args) == 3:
            self.start, self.stop, self.step = args
        else:
            raise TypeError
        self.ptr = self.start

    def __iter__(self):
        return self

    def __next__(self):
        access = self.ptr < self.stop and self.step > 0
        access = access or (self.ptr > self.stop and self.step < 0)
        if access:
            ptr = self.ptr
            self.ptr += self.step
            return ptr
        else:
            raise StopIteration()

    def __contains__(self, item):
        if self.step > 0:
            return self.start <= item < self.stop
        else:
            return self.start >= item > self.stop

    def __len__(self):
        return abs(self.stop - self.start) // self.step

    def __getitem__(self, item):
        return self.start + item * self.step

    def __repr__(self):
        if self.step == 1:
            return "Range(%d, %d)" % (self.start, self.stop)
        else:
            return "Range(%d, %d, %d)" % (self.start, self.stop, self.step)

