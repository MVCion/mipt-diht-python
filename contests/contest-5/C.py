def profiler(to_wrap):
    import functools
    from time import time

    to_wrap.__depth, to_wrap.calls = 1, 0

    @functools.wraps(to_wrap)
    def wrapper(*args, **kwargs):
        wrapper.calls += 1
        execution_begin = time()
        if to_wrap.__depth == wrapper.calls:
            value = to_wrap(*args, **kwargs)
            wrapper.calls -= to_wrap.__depth - 1
            to_wrap.__depth = wrapper.calls + 1
        else:
            value = to_wrap(*args, **kwargs)
        wrapper.last_time_taken = time() - execution_begin
        return value

    return wrapper
