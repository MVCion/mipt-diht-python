import collections
import functools


def cache(cache_size):
    class KeyCache:

        def __init__(self, key_cache_size):
            self.__cache_size = key_cache_size
            self.__cache = collections.OrderedDict()

        def __contains__(self, key):
            return key in self.__cache

        def add(self, key, value):
            self.__cache[key] = value
            if len(self.__cache) > self.__cache_size:
                self.__cache.popitem(last=False)

        def get(self, key):
            return self.__cache.get(key)

    def outter_wrapper(to_wrap):
        key_cache = KeyCache(cache_size)

        @functools.wraps(to_wrap)
        def wrapper(*args, **kwargs):
            key = tuple(list(args) + list(tuple(kwargs.items())))
            if key not in key_cache:
                key_cache.add(key, to_wrap(*args, **kwargs))
            return key_cache.get(key)

        return wrapper

    return outter_wrapper
