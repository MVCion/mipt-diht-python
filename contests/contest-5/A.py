import functools
import sys


def takes(*types):
    def outter_wrapper(to_wrap):

        @functools.wraps(to_wrap)
        def inner_wrapper(*args, **kwargs):

            for entity in zip(args, types):
                if not isinstance(entity[0], entity[1]):
                    raise TypeError
            return to_wrap(*args, **kwargs)

        return inner_wrapper

    return outter_wrapper


exec(sys.stdin.read())
