class Player:
    def __format__(self, start, ready, sn='new_start', rn='new_ready'):
        setattr(self, sn, start)
        setattr(self, rn, ready)


def play(game):
    player = Player()
    ready_method, on_start_method = 'ready', 'start'
    setattr(player, 'ready', player.__format__)
    while True:
        game_method = getattr(game, on_start_method)
        game_method(player)
        on_start_method = player.new_start
        delattr(player, 'new_start')
        setattr(player, player.new_ready, getattr(player, ready_method))
        delattr(player, ready_method)
        ready_method = player.new_ready
        delattr(player, 'new_ready')
