import sys

import copy


class ExtendedList(list):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @property
    def reversed(self):
        return self[::-1]

    R = reversed

    def get_first(self):
        return self[0]

    def set_first(self, value):
        self[0] = value

    first = property(get_first, set_first)

    F = first

    def get_last(self):
        return self[-1]

    def set_last(self, value):
        self[-1] = value

    last = property(get_last, set_last)

    L = last

    def get_size(self):
        return len(self)

    def set_size(self, new_size):
        if self.get_size() < new_size:
            self.extend([None for _ in range(new_size - self.get_size())])
        else:
            self[:] = copy.deepcopy(self[0:new_size])

    size = property(get_size, set_size)

    S = size


exec(sys.stdin.read())
