import json

RACES = {
    "Sorceress": {
        "health": 50,
        "defence": 42,
        "attack": 90,
        "mana": 200
    },
    "Knight": {
        "health": 100,
        "defence": 170,
        "attack": 150,
        "mana": 0
    },
    "Barbarian": {
        "health": 120,
        "defence": 150,
        "attack": 180,
        "mana": 0
    },
    "Warlock": {
        "health": 70,
        "defence": 50,
        "attack": 100,
        "mana": 180
    }
}


class Unit:

    def __init__(self, unit):
        self.health = int(unit["health"])
        self.attack_power = int(unit["attack"])
        self.defence_power = int(unit["defence"])
        self.mana = 0 if unit.get("mana") is None else int(unit["mana"])
        self.exp = int(unit["experience"])
        self.race = unit["race"]
        self.lord = unit["lord"]

    @property
    def bonus(self):
        return 0 if self.health == 0 else self.exp + \
            2 * self.defence_power + 3 * self.attack_power + 10 * self.mana

    def interact(self, cls, action, power):
        if self.health * cls.health == 0:
            return
        self.exp += 1
        if action == "attack":
            self.__attack(cls, power)
        elif action == "cast_health_spell":
            self.__cast_health_spell(cls, power)
        elif action == "cast_damage_spell":
            self.__cast_damage_spell(cls, power)
        else:
            raise EnvironmentError

    def __make_damage(self, cls, power):
        cls.exp += 1
        new_defence = cls.defence_power - power
        if new_defence < 0:
            cls.defence_power = 0
            cls.health = max(0, cls.health + new_defence)
            if cls.health == 0:
                self.exp += 4
        else:
            cls.defence_power = new_defence

    def __attack(self, cls, power):
        self.attack_power -= power
        self.__make_damage(cls, power)

    def __cast_health_spell(self, cls, power):
        self.mana -= power
        cls.health = max(cls.health + power, RACES[cls.race]["health"])

    def __cast_damage_spell(self, cls, power):
        self.mana -= power
        self.__make_damage(cls, power)


class Game:

    def __init__(self, battle):
        armies = battle.get('armies')
        self.units = {}
        for unit_id in armies.keys():
            self.units[unit_id] = Unit(armies[unit_id])
        self.battle_steps = battle.get('battle_steps')

    def __generate(self):
        for battle_step in self.battle_steps:
            active_unit = self.units[battle_step["id_from"]]
            passive_unit = self.units[battle_step["id_to"]]
            action = battle_step["action"]
            power = int(battle_step["power"])
            active_unit.interact(passive_unit, action, power)

    @property
    def winner(self):
        self.__generate()
        total_score = {
            "Archibald": 0,
            "Ronald": 0
        }
        for unit in self.units.values():
            if unit.health > 0:
                total_score[unit.lord] += unit.bonus
        if total_score["Archibald"] > total_score["Ronald"]:
            return "Archibald"
        elif total_score["Archibald"] < total_score["Ronald"]:
            return "Ronald"
        else:
            return "unknown"


if __name__ == '__main__':
    decoder = json.JSONDecoder()
    battle_history = decoder.decode(input())
    game = Game(battle_history)
    print(game.winner)
