keys = {"-l": False, "-w": False, "-m": False, "-L": False}
with open("input.txt", "r") as infile:
    w, m, L = 0, 0, 0
    for l, line in enumerate(infile):
        if l == 0:
            for key in line.split():
                keys[key] = True
        else:
            w += len(line.split())
            m += len(line)
            L = max(L, len(line) - 1)
for key, value in [('-l', l), ('-w', w), ('-m', m), ('-L', L)]:
    if keys[key]:
        print(value, end=" ")
