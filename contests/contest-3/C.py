import re


def logformat(log):
    logdata = log.split()
    sha = logdata[0][0:7]
    email_index = -1
    for i, s in enumerate(logdata):
        if re.findall(r'[\w\.-]+@[\w\.-]+', s):
            email_index = i
            break
    message_start = log.find(logdata[email_index + 1])
    message = log[message_start:]
    return "{0}{1}{2}".format(sha, ("." * (73 - len(message))), message)


with open("input.txt", "r") as infile:
    with open("output.txt", "w") as outfile:
        for log in infile:
            outfile.write(logformat(log.strip()) + "\n")
