
import string
import re

password, strong = input(), True
if re.match("anna", password.lower()):
    strong = False
if len(password) < 8 or len(set(password)) < 4:
    strong = False
s = set(password)
if not s & set(string.digits):
    strong = False
if not s & set(string.ascii_lowercase):
    strong = False
if not s & set(string.ascii_uppercase):
    strong = False

print("strong" if strong else "weak")
