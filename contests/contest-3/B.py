
def ispalindrome(s):
    return s == s[::-1]

s = input()
for i in range(len(s)+1):
    a, b = s + s[0:i][::-1], s[len(s)-i:len(s)][::-1] + s
    if ispalindrome(a) or ispalindrome(b):
        print(i)
        break
