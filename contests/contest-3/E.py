
s = input()
t = list(s)
for i, c in enumerate(s):
    if c == 'T':
        t[i] = 'A'
    if c == 'A':
        t[i] = 'T'
    if c == 'C':
        t[i] = 'G'
    if c == 'G':
        t[i] = 'C'
print("".join(t)[::-1])
