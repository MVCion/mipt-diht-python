penguin = [
    '   _~_    ',
    '  (o o)   ',
    ' /  V  \  ',
    '/(  _  )\ ',
    '  ^^ ^^   '
]

n = int(input())
for level in penguin:
    print(level * n)
