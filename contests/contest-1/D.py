def fibonacci(index):
    prev, current = 0, 1
    for i in range(index):
        prev, current = current, prev + current
    return prev

index = int(input())
print(fibonacci(index))
