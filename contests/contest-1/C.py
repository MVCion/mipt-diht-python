def exchange(value, denominations=[1, 5, 10]):
    if value == 0:
        return 1
    elif value < 0 or (not denominations and value != 0):
        return 0
    else:
        return exchange(value - denominations[0], denominations) +\
            exchange(value, denominations[1:])

value = int(input())
print(exchange(value))
