def get_nearest_lucky(ticket):

    def is_lucky(ticket):
        first_half, second_half = 0, 0
        for i in range(3):
            second_half += ticket % 10
            ticket //= 10
        for i in range(3):
            first_half += ticket % 10
            ticket //= 10
        return first_half == second_half

    lucky_found, difference = False, 0
    while not lucky_found:
        if is_lucky(ticket - difference):
            ticket, lucky_found = ticket - difference, True
        if is_lucky(ticket + difference):
            ticket, lucky_found = ticket + difference, True
        difference += 1
    return ticket

ticket = int(input())
print(get_nearest_lucky(ticket))
