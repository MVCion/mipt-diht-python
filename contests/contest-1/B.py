
def replace(index):

    if index % 15 == 0:
        return "Fizz Buzz"
    elif index % 3 == 0:
        return "Fizz"
    elif index % 5 == 0:
        return "Buzz"
    else:
        return str(index)

n = int(input())
print(", ".join([replace(i) for i in range(1, n + 1)]))
