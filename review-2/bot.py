import json
import os

import telebot

import analytics
import config
import db_manager
import logger
import search_engine


engine = search_engine.SearchEngine()

with open('commands.json', encoding='utf-8') as bot_activity_file:
    bot_activity = json.loads(bot_activity_file.read())

bot = telebot.TeleBot(config.URBAN_BOT_TOKEN, threaded=True)
bot.remove_webhook()


@bot.message_handler(commands=['start', 'help'])
def handle_start_help(message):
    logger.bot_logger.info("%s: %s" % (message.chat, message.text))
    lang = db_manager.get_lang(message.chat.id)
    bot.send_message(message.chat.id, bot_activity['commands'][lang][message.text])


@bot.message_handler(commands=['statistics'])
def handle_statistics(message):
    logger.bot_logger.info("%s: %s" % (message.chat, message.text))
    lang = db_manager.get_lang(message.chat.id)
    bot.send_message(message.chat.id, bot_activity['commands'][lang][message.text])
    
    lang_fname = analytics.language_frequency(message.chat.id)
    with open(lang_fname, 'rb') as lang_bar_chart:
        bot.send_message(message.chat.id, bot_activity['commands'][lang]["statistics"]["users"])
        bot.send_photo(message.chat.id, lang_bar_chart)
    os.remove(lang_fname)
    
    req_fname = analytics.request_frequency(message.chat.id)
    with open(req_fname, 'rb') as req_bar_chart:
        bot.send_message(message.chat.id, bot_activity['commands'][lang]["statistics"]["terms"])
        bot.send_photo(message.chat.id, req_bar_chart)
    os.remove(req_fname)


@bot.message_handler(commands=['top'])
def get_top(message):
    logger.bot_logger.info("%s: %s" % (message.chat, message.text))
    lang = db_manager.get_lang(message.chat.id)
    if len(message.text.split(' ')) != 2:
        bot.send_message(message.chat.id, bot_activity['commands'][lang]["top_error"])
    else:
        _, limit = message.text.split(' ')
        try:
            limit = int(limit)
            for response in engine.get_top(limit, lang):
                bot.send_message(message.chat.id, response)
        except ValueError:
            bot.send_message(message.chat.id, bot_activity['commands'][lang]["top_error"])


@bot.message_handler(commands=['lang'])
def handle_lang(message):
    keyboard = telebot.types.InlineKeyboardMarkup()
    for lang_alias, language in bot_activity["vocabulary"].items():
        keyboard.add(telebot.types.InlineKeyboardButton(text=language, callback_data=lang_alias))
    logger.bot_logger.info("%s: %s" % (message.chat, message.text))
    lang = db_manager.get_lang(message.chat.id)
    bot.send_message(message.chat.id,
                     bot_activity['commands'][lang][message.text],
                     reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    db_manager.set_lang(call.message.chat.id, call.data)
    bot.edit_message_text(chat_id=call.message.chat.id,
                          message_id=call.message.message_id,
                          text=bot_activity["commands"][call.data]["lang_callback"])


@bot.message_handler(content_types=['text'])
def get_explanation(message):
    logger.bot_logger.info("%s: %s" % (message.chat, message.text))
    explanation = engine.search(message.text, lang=db_manager.get_lang(message.chat.id))
    logger.bot_logger.info('Send to %s: %s...'
                           % (message.chat.id, explanation[:min(140, len(explanation))]))
    # The message may not fit into the size of 4096 bytes
    for position in range(len(explanation) // (2 ** 12) + 1): 
        from_position = position * (2 ** 12)
        to_position = min(len(explanation), (position + 1) * (2 ** 12))
        bot.send_message(message.chat.id, explanation[from_position:to_position])

bot.polling(none_stop=True)
