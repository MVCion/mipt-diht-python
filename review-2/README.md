## Второе ревью по дисциплине "Программирование на Python" ##
# UrbanBot - Телеграм-бот для ресурса Urban Dictionary с поддержкой различных языков #

###### Описание проекта: ######

      Создан Телеграм-бот (@botobotbot), который находит толкование слов на ресурсе urbandictionary.com 
      среди самых популярных ответов. Особенность бота - поддержка множества языков в отличии от самого 
      сервиса (доступна только английская версия), что открывает сайт для большей  аудитории. Также 
      добавлена простая инфорграфика и возможность узнать самые популярные запросы среди пользователей.
	  
___

###### Техническая часть: ######

      Используемые API:
      
      * pyTelegramBotAPI (логика самого бота);
      * YandexTranslateAPI (для выполнения переводов).
      
      Сторонние библиотеки:
      
      * bs4 (скраппинг)
      * matplotlib и numpy (инфографика)
      * telebot (сам бот)
      * requests (для скраппинга и работы с переводчиком)
      
      Встроенные библиотеки:
      
      * datetime
      * logging
      * json
      * os
      * sqlite3
      * tkinter
      
      Локальные модули:
      
      * analitycs (инфографика)
      * bot 
      * config 
      * db_manager (для работы с базой данных)
      * logger
      * scrappers (скраппер для urbandictionary.com)
      * search_engine (объединение скраппинга и Yandex Translate)
      * translate (библиотека для работы с YandexTranslateAPI)
      
___

###### Дополнительно ######
      
Также создан вебхук с использованием Flask и Digital Ocean.